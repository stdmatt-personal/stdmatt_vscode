//----------------------------------------------------------------------------//
// Imports                                                                    //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
import * as vscode from "vscode";
import * as fs     from "fs";
import * as path   from "path";
//------------------------------------------------------------------------------
const strftime = require("strftime");


//----------------------------------------------------------------------------//
// Ruler Info                                                                 //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
export class Ruler_Info
{
    public rulers: number[];

    constructor(...args)
    {
        this.rulers = [...args];
        // @todo(stdmatt): [Maybe we wanna sort?] - 21 Dec, 2021 at 09:07:26
        if(this.rulers.length == 0) {
            this.rulers.push(80);
        }
    }
}

//----------------------------------------------------------------------------//
// Comment Info                                                               //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
export class Comment_Info
{
    public start: string;
    public end:   string;
    public line:  string;

    constructor(start: string, end: string, line: string)
    {
        this.start = start;
        this.end   = end;
        this.line  = line;
    }
}


//----------------------------------------------------------------------------//
// VS Utils                                                                   //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
export class VS_Utils
{
    //------------------------------------------------------------------------//
    // Constants                                                              //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static DEFAULT_COMMENT_INFO = new Comment_Info("//", "//", "//");
    static DEFAULT_EDITOR_RULER = new Ruler_Info(80);
    static DEFAULT_CONFIG_KEY   = "stdmatt_vscode";


    //------------------------------------------------------------------------//
    // Editor                                                                 //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static get_editor(): vscode.TextEditor | null
    {
        const editor = vscode.window.activeTextEditor;
        return (editor) ? editor : null;
    }


    //------------------------------------------------------------------------//
    // Workspace                                                              //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static get_workspace(): any
    {
        return vscode.workspace;
    }


    //------------------------------------------------------------------------//
    // Document                                                               //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static get_current_document(): vscode.TextDocument | null
    {
        const editor = VS_Utils.get_editor();
        if(!editor) {
            return null;
        }

        return editor.document;
    }

    //--------------------------------------------------------------------------
    static get_document_path(document: vscode.TextDocument): string
    {
        const doc_path = document.uri.fsPath;
        return doc_path;
    }

    //--------------------------------------------------------------------------
    static get_document_filename(document: vscode.TextDocument): string
    {
        const doc_path     = VS_Utils.get_document_path(document);
        const doc_filename = path.basename(doc_path);
        return doc_filename;
    }

    //--------------------------------------------------------------------------
    static get_document_dirname(document: vscode.TextDocument): string
    {
        const doc_path = VS_Utils.get_document_path(document);
        const doc_dir  = path.dirname(doc_path);
        return doc_dir;
    }

    static get_current_comment(): Comment_Info
    {
        const document = VS_Utils.get_current_document();
        const comment  = VS_Utils.get_comment(document);

        return comment;
    }

    //--------------------------------------------------------------------------
    static get_comment(document: vscode.TextDocument | null): Comment_Info
    {
        if(!document) {
            return VS_Utils.DEFAULT_COMMENT_INFO;
        }

        const doc_path = VS_Utils.get_document_path(document);
        const doc_ext  = path.extname(doc_path);
        switch(doc_ext)
        {
            case ".c":
            case ".h":
            case ".css":
                return new Comment_Info("/*", "*/",  "");
            case ".cpp":
            case ".hpp":
            case ".js":
            case ".ts":
            case ".mjs":
                return new Comment_Info("/*", "*/",  "//");

            case ".ps1":
            case ".sh":
                return new Comment_Info("##", "##", "##");

            case ".html":
                return new Comment_Info("<!-- ", " -->", "");
        }

        return VS_Utils.DEFAULT_COMMENT_INFO;
    }

    //--------------------------------------------------------------------------
    static get_ruler() : Ruler_Info
    {
        const workspace = VS_Utils.get_workspace();
        if(!workspace) {
            return VS_Utils.DEFAULT_EDITOR_RULER;
        }

        const workspace_config = workspace.getConfiguration("editor");
        if(!workspace_config) {
            return VS_Utils.DEFAULT_EDITOR_RULER;
        }

        const rulers = workspace_config["rulers"];
        if(!rulers || rulers.length <= 0) {
            return VS_Utils.DEFAULT_EDITOR_RULER;
        }

        return new Ruler_Info(...rulers);
    }

    //--------------------------------------------------------------------------
    static get_selection(): vscode.Selection | null
    {
        const editor = VS_Utils.get_editor();
        if(!editor) {
            return null;
        }
        return editor.selection;
    }


    //------------------------------------------------------------------------//
    // Config                                                                 //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static get_config(key = VS_Utils.DEFAULT_CONFIG_KEY)
    {
        const workspace = VS_Utils.get_workspace();
        if(!workspace) {
            return null;
        }

        const workspace_folders = workspace.workspaceFolders;
        if(workspace_folders && workspace_folders.length <= 0) {
            return null;
        }

        // Get the Color from what user has configured.
        const workspace_config = workspace.getConfiguration(key);
        if(!workspace_config) {
            return null;
        }

        return workspace_config;
    }

    //--------------------------------------------------------------------------
    // The currently selected text or the empty string
    static get_tm_selected_text(): string
    {
        const selection = VS_Utils.get_selection();
        if(!selection) {
            return "";
        }

        const range = new vscode.Range(selection.start, selection.end);
        const text  = VS_Utils.get_current_document()?.getText(range);
        return (text) ? text : "";
    }

    //--------------------------------------------------------------------------
    // The contents of the current line
    static get_tm_current_line(): string
    {
        const selection = VS_Utils.get_selection();
        if(!selection) {
            return "";
        }

        const document = VS_Utils.get_current_document();
        if(!document) {
            return "";
        }

        const text = document.lineAt(selection.active.line);
        return (text) ? text.text : "";
    }

    //--------------------------------------------------------------------------
    // The contents of the word under cursor or the empty string
    static get_tm_current_word(): string
    {
        // @todo(stdmatt): [IMPLEMENT] - 23 Dec, 2021 at 03:02:41
        return "";
    }

    //--------------------------------------------------------------------------
    // The zero-index based line number
    static get_tm_line_index(): number
    {
        const selection = VS_Utils.get_selection();
        if(!selection) {
            return 0;
        }
        return selection.active.line;
    }

    //--------------------------------------------------------------------------
    // The one-index based line number
    static get_tm_line_number(): number
    {
        const selection = VS_Utils.get_selection();
        if(!selection) {
            return 1;
        }
        return selection.active.line + 1;
    }

    //--------------------------------------------------------------------------
    // The filename of the current document
    static get_tm_filename(): string
    {
        const filepath = VS_Utils.get_tm_filepath();
        const basename = path.basename(filepath);
        return basename;
    }

    //--------------------------------------------------------------------------
    // The filename of the current document without its extensions
    static get_tm_filename_base(): string
    {
        const filepath = VS_Utils.get_tm_filepath();
        const basename = path.extname(path.basename(filepath));
        return basename;
    }

    //--------------------------------------------------------------------------
    // The directory of the current document
    static get_tm_directory(): string
    {
        const filepath = VS_Utils.get_tm_filepath();
        const basename = path.dirname(filepath);
        return basename;
    }

    //--------------------------------------------------------------------------
    // The full file path of the current document
    static get_tm_filepath(): string
    {
        const document = VS_Utils.get_current_document();
        if(!document) {
            return "";
        }

        const filepath = document.fileName;
        return filepath;
    }

    //--------------------------------------------------------------------------
    // The relative (to the opened workspace or folder) file path of the current document
    static get_relative_filepath(): string
    {
        // @todo(stdmatt): [IMPLEMENT] - 23 Dec, 2021 at 03:10:27
        return "";
    }

    //--------------------------------------------------------------------------
    static get_clipboard(): string
    {
        // @todo(stdmatt): [IMPLEMENT] - 23 Dec, 2021 at 03:10:27
        return "";
    }

    //--------------------------------------------------------------------------
    // The name of the opened workspace or folder
    static get_workspace_name(): string
    {
        const workspace = VS_Utils.get_workspace();
        if(!workspace) {
            return "";
        }

        const name = workspace.name;
        if(!name) {
            return "";
        }

        return name;
    }

    //--------------------------------------------------------------------------
    // The path of the opened workspace or folder
    static get_workspace_folder(): string
    {
        const workspace = VS_Utils.get_workspace();
        if(!workspace) {
            return "";
        }

        return workspace.rootPath;
    }

    //--------------------------------------------------------------------------
    // The current year
    static get_current_year(): string
    {
        return strftime("%Y");
    }

    //--------------------------------------------------------------------------
    // The current year's last two digits
    static get_current_year_short(): string
    {
        return strftime("%y");
    }

    //--------------------------------------------------------------------------
    // The full name of the month (example 'July')
    static get_current_month(): string
    {
        return strftime("%m");
    }

    //--------------------------------------------------------------------------
    static get_current_month_name(): string
    {
        return strftime("%B");
    }

    //--------------------------------------------------------------------------
    // The short name of the month (example 'Jul')
    static get_current_month_name_short(): string
    {
        return strftime("%b");
    }

    //--------------------------------------------------------------------------
    // The day of the month as two digits (example '08')
    static get_current_date(): string
    {
        return strftime("%d");
    }

    //--------------------------------------------------------------------------
    // The name of day (example 'Monday')
    static get_current_day_name(): string
    {
        return strftime("%A");
    }

    //--------------------------------------------------------------------------
    // The short name of the day (example 'Mon')
    static get_current_day_name_short(): string
    {
        return strftime("%a");
    }

    //--------------------------------------------------------------------------
    // The current hour in 24-hour clock format
    static get_current_hour(): string
    {
        return strftime("%H");
    }

    //--------------------------------------------------------------------------
    // The current minute as two digits
    static get_current_minute(): string
    {
        return strftime("%M");
    }

    //--------------------------------------------------------------------------
    // The current second as two digits
    static get_current_second(): string
    {
        return strftime("%S");
    }

    //--------------------------------------------------------------------------
    static get_current_seconds_unix(): string
    {
        return Date.now().toString();
    }

    //--------------------------------------------------------------------------
    static get_random(): number
    {
        const value = (999999 * Math.random());
        return value;
    }

    //--------------------------------------------------------------------------
    static get_random_hex(): number
    {
        const value = (0xFFFFFF * Math.random());
        return value;
    }

    //--------------------------------------------------------------------------
    static get_uuid(): string
    {
        // @todo(stdmatt): [IMPLEMENT] - 23 Dec, 2021 at 03:38:33
        return "";
    }

    //--------------------------------------------------------------------------
    // Example output: in PHP /* or in HTML <!--
    static get_block_comment_start(): string
    {
        const document = VS_Utils.get_current_document();
        const comment  = VS_Utils.get_comment(document);
        return comment.start;
    }

    //--------------------------------------------------------------------------
    // Example output: in PHP */ or in HTML -->
    static get_block_comment_end(): string
    {
        const document = VS_Utils.get_current_document();
        const comment  = VS_Utils.get_comment(document);
        return comment.end;
    }

    //--------------------------------------------------------------------------
    // Example output: in PHP //
    static get_line_comment(): string
    {
        const document = VS_Utils.get_current_document();
        const comment  = VS_Utils.get_comment(document);
        return comment.line;
    }
}